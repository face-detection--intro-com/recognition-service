package com.introcom.recognitionservice.model;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="LogEntry")
public class LogEntry {
    @Id
    @ApiModelProperty(notes = "Randomly generated UUID")
    private String id;
    @ApiModelProperty(notes = "The camera ID of the camera that sent the report")
    private int cameraId;
    @ApiModelProperty(notes = "The time the picture was taken in epoch milliseconds")
    private long utcTime;
    @ApiModelProperty(notes = "The picture taken in base64")
    private String image;
    @ApiModelProperty(notes = "The ID of the person recognised")
    private String personId;
    @ApiModelProperty(notes = "The name of the person recognised")
    private String personName;
    @ApiModelProperty(notes = "The name of the camera that took the picture")
    private String cameraName;

    public LogEntry() {
    }

    public LogEntry(String id, int cameraId, long utcTime, String image, String personId) {
        this.id = id;
        this.cameraId = cameraId;
        this.utcTime = utcTime;
        this.image = image;
        this.personId = personId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCameraId() {
        return cameraId;
    }

    public void setCameraId(int cameraId) {
        this.cameraId = cameraId;
    }

    public long getUtcTime() {
        return utcTime;
    }

    public void setUtcTime(long utcTime) {
        this.utcTime = utcTime;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getCameraName() {
        return cameraName;
    }

    public void setCameraName(String cameraName) {
        this.cameraName = cameraName;
    }
}
