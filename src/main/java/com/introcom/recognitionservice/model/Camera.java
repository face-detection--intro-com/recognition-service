package com.introcom.recognitionservice.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class Camera {
    @Id
    private String id;
    private Long cameraId;
    private String name;
    private String brand;
    private String model;

    private Boolean active;

    public Camera() {
        this.active = true;
    }

    public Camera(Long cameraId, String name, String brand, String model) {
        this.cameraId = cameraId;
        this.name = name;
        this.brand = brand;
        this.model = model;
        this.active = true;
    }

    public String getId() {
        return id;
    }

    public Long getCameraId() {
        return cameraId;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public void desactive() {
        this.active = false;
    }

    public void active() {
        this.active = true;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public void setId(String id) {
        this.id = id;
    }
}
