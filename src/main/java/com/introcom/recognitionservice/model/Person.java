package com.introcom.recognitionservice.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Person {

    @Id
    public String id;
    public String name;
    public String face;
    public Integer lastCameraSeen;
    public Long lastUtcTimeSeen;

    public Person() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public Integer getLastCameraSeen() {
        return lastCameraSeen;
    }

    public void setLastCameraSeen(Integer lastCameraSeen) {
        this.lastCameraSeen = lastCameraSeen;
    }

    public Long getLastUtcTimeSeen() {
        return lastUtcTimeSeen;
    }

    public void setLastUtcTimeSeen(Long lastUtcTimeSeen) {
        this.lastUtcTimeSeen = lastUtcTimeSeen;
    }
}
