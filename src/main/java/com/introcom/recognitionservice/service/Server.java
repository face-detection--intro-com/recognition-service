package com.introcom.recognitionservice.service;

import com.introcom.recognitionservice.client.ChangonitionClient;
import com.introcom.recognitionservice.model.Camera;
import com.introcom.recognitionservice.model.LogEntry;
import com.introcom.recognitionservice.model.Person;
import com.introcom.recognitionservice.protocol.CameraMessage;
import com.introcom.recognitionservice.protocol.Cameramessage;
import com.introcom.recognitionservice.repository.CameraRepository;
import com.introcom.recognitionservice.repository.LogRepository;
import com.introcom.recognitionservice.repository.PersonRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Component
public class Server extends Thread {
    @Autowired
    private LogRepository logRepository;
    @Autowired
    private ChangonitionClient changonitionClient;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    CameraRepository cameraRepository;

    final private int PORT = 6000;
    private ServerSocket serverSocket;


    public Server() {
        try {
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        boolean running = true;

        try {
            System.out.println("Esperando conexion en puerto: " + PORT);
            Socket clientSocket = serverSocket.accept();
            InputStream inputStream = clientSocket.getInputStream();
            while (running) {
                Cameramessage message = Cameramessage.parseDelimitedFrom(inputStream);
                if (message != null) {
                    System.out.println("Got message");
                    Person person = new Person();
                    person.setFace(message.getBase64Image());
                    try {
                        String id = changonitionClient.getIdForFace(person);
                        LogEntry log = new LogEntry();
                        if(!id.equals("-1")){// No es un NN
                            id = id.substring(1,id.length()-1); //Changonition lo envia con comillas escapadsas (\"ID\")
                            Optional<Person> update = personRepository.findById(id);
                            if(update.isPresent()) {
                                update.get().setLastCameraSeen(message.getId());
                                //update.get().setLastUtcTimeSeen(message.getUtcTime());
                                update.get().setLastUtcTimeSeen(new Date().getTime());
                                personRepository.save(update.get());
                                log.setPersonName(update.get().getName());
                            }
                        }
                        Optional<Camera> camera = cameraRepository.findByCameraId((long) message.getId());
                        camera.ifPresent(value -> log.setCameraName(value.getName()));
                        log.setCameraId(message.getId());
                        //log.setUtcTime(message.getUtcTime());
                        log.setUtcTime(new Date().getTime());
                        log.setImage(message.getBase64Image());
                        log.setPersonId(id);
                        log.setId(UUID.randomUUID().toString());
                        logRepository.save(log);

                    } catch (FeignException f) {
                        System.out.println("Changonition failed");
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                serverSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
