package com.introcom.recognitionservice.repository;

import com.introcom.recognitionservice.model.LogEntry;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface LogRepository extends MongoRepository<LogEntry,String> {
   List<LogEntry> findTop10ByOrderByUtcTimeDesc();
}
