package com.introcom.recognitionservice.repository;

import com.introcom.recognitionservice.model.Camera;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CameraRepository extends CrudRepository<Camera, Long> {
    Optional<Camera> findByCameraId(Long id);
}
