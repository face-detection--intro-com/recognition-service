package com.introcom.recognitionservice.repository;

import com.introcom.recognitionservice.model.Person;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PersonRepository extends MongoRepository<Person,String> {
}
