package com.introcom.recognitionservice.controller;

import com.introcom.recognitionservice.model.LogEntry;
import com.introcom.recognitionservice.repository.LogRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController

public class LogController {
    @Autowired
    private LogRepository logRepository;

    @GetMapping("/")
    @ApiOperation(value = "Get the last 10 log entries")
    public List<LogEntry> getLast10Logs(){
        return logRepository.findTop10ByOrderByUtcTimeDesc();
    }
}
