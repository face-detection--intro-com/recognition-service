package com.introcom.recognitionservice;

import com.introcom.recognitionservice.repository.LogRepository;
import com.introcom.recognitionservice.repository.PersonRepository;
import com.introcom.recognitionservice.service.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableFeignClients
@EnableSwagger2
public class PluginRecognitionApplication {

    @Autowired
    Server server;

    public static void main(String[] args) {
        SpringApplication.run(PluginRecognitionApplication.class, args);
    }

    @Bean
    public CommandLineRunner run(){
        return args -> {
            server.start();
        };
    }

    @Bean
    public Docket swaggerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.introcom.recognitionservice.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(new ApiInfoBuilder().version("1.0").title("Recognition API").description("Documentation for Recognition-service API v1.0").build());
    }
}
