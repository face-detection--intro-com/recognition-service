package com.introcom.recognitionservice.client;

import com.introcom.recognitionservice.model.Person;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "Changonition", url = "http://localhost:8000/changonition/api/v1/person")
public interface ChangonitionClient {

    @PostMapping("/")
    public Person createPerson(@RequestBody Person person);
    @PutMapping("/")
    public void updatePerson(@RequestBody Person person);
    @DeleteMapping("/")
    public void deletePerson(@RequestBody Person person);
    @PostMapping("/get_index/")
    public String getIdForFace(@RequestBody Person person);
}
